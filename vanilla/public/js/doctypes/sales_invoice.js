frappe.ui.form.on('Sales Invoice', {
    validate: function(frm) {
        if (frm.doc.is_pos) {
            const payments = frm.doc.payments.filter((d) => d.mode_of_payment === 'Customer Balance');
            const reducer = (acc, value) => {
                const v = value.amount || 0;
                return acc + v;
            };
            const amounts = payments.reduce(reducer, 0);

            if (!frm.doc.is_return && amounts < 0) {
                frappe.throw(__('Customer Balance mode of payment cannot be negative.'));
            }

            if (!frm.doc.is_return && amounts > frm.doc.grand_total) {
                frappe.throw(__('Customer Balance mode of payment amount must not be more than the invoice amount'));
            }

            if (amounts > frm.doc.customer_balance) {
                frappe.throw(
                    __(`
                    ${frm.doc.customer}'s debtor balance is ${frm.doc.customer_balance}.
                    Customer Balance mode of payment amount must not exceeed 
                    ${frm.doc.customer_balance}.
                    `));
            }
        }
    },
    customer: function(frm) {
		if (frm.doc.customer) {
			frappe.call({
				method: 'vanilla.utils.get_customer_balance',
				args: {'party': frm.doc.customer},
				callback: function(r) {
					if (r.message) {
                        frm.set_value('customer_balance', r.message.balance);
					}
				}
			});
		} else {
			frm.set_value('customer_balance', 0);
		}
	},
});
